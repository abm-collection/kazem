% -*-trale-prolog-*-

% use ghostview for drawing signatures
graphviz_option(ps,gv).
%graphviz_option(svg,squiggle).

% Load phonology and tree output

:- [trale_home(tree_extensions)].

>>> onset.
onset <<< nucl.
nucl <<< atr.
atr <<< long.
long <<< coda.

>>> sing.
sing <<< plur.
plur <<< phon.

% specify signature file
signature(signature).

% load lexicon
:- [lexicon].

% load relational constraints for rules
:- [constraints].



