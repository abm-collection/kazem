% -*-trale-prolog-*-

%% macros

ph(C) := (consonant, core:C).
vow(C) := (vowel, core:C).
vow_sn(C) := (vowel, core:C, long:minus).
vow_ln(C) := (vowel, core:C, long:plus).

vow_s(C, ATR) := (vowel, core:C, long:minus, atr:ATR).
vow_gl(G, C, ATR) := (vowel, core_g:G, core:C, long:plus, atr:ATR).
vow_l(C, ATR) := (vowel, core:C, long:plus, atr:ATR).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%                  plural formation in Kazem
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

sg_x(X) := (word, sing:append(F,[(syll, nucl: @vow_sn(X))])).
pl_x(X) := (word, plur:append(F,[(syll, nucl: @vow_sn(X))])).

sg_ox(X) := (word, sing:append(F,[(syll, onset: @ph(X))])).
pl_ox(X) := (word, plur:append(F,[(syll, onset: @ph(X))])).

sg_cx(X) := (word, sing:append(F,[(syll, coda: @ph(X))])).
pl_cx(X) := (word, plur:append(F,[(syll, coda: @ph(X))])).

%% onset of the antepenultimate is equal

no_length *> (word,
	      ((sing:append(F, [(ecv, nucl: (vowel, long:L)), (syll)]),
		plur:append(F, [(ecv, nucl: (vowel, long:L)), (syll)]));
	       (sing:append(F, [(syll, nucl: (vowel, long:L), coda: C), (syll)]),
		plur:append(F, [(syll, nucl: (vowel, long:L), coda: C), (syll)]));
	       (sing:append(F, [(syll, nucl: (vowel, long:L))]),
		plur:append(F, [(ecv, nucl: (vowel, long:L)), (syll)]));
	       (sing:append(F, [(syll, nucl: (vowel, long:L), coda: C)]),
		plur:append(F, [(syll, nucl: (vowel, long:L), coda: C), (syll)]));
	       (sing:append(F, [(ecv, nucl: (vowel, long:L)), (syll)]),
		plur:append(F, [(syll, nucl: (vowel, long:L))])))).

no_dp *> (word,
	  ((sing:append(F,[(syll, nucl:(vowel, core:C)), (syll)]),
	    plur:append(F,[(syll, nucl:(vowel, core:C)), (syll)]));
	   (sing:append(F,[(syll, nucl:(vowel, core:C))]),
	    plur:append(F,[(syll, nucl:(vowel, core:C)), (syll)]));
	   (sing:append(F,[(syll, nucl:(vowel, core:C)), (syll)]),
	    plur:append(F,[(syll, nucl:(vowel, core:C))])))).

%% lengthening

length_vv *> (length,
	      plur:append(F,[(syll, nucl: (phoneme, long:plus)), (cv)])).

length_cc *> (length,
	      plur:append(F,[(syll, coda: O, nucl:long:minus), (syll, onset: O)])).

%% diphthong

has_dp *> (word,
	   ((sing:append(F,[(syll, nucl:(vowel, core: o)), (cv)]),
	     plur:append(F,[(syll, nucl:(vowel, core: wa)), (cv)]));
	    (sing:append(F,[(syll, nucl:(vowel, core: i)), (cv)]),
	     plur:append(F,[(syll, nucl:(vowel, core: we)), (cv)])))).

%% alternations

v1_v2 *> (word,
	  sing:append(F,[(syll), (cv, onset:X)]),
	  plur:append(F,[(syll), (cv, onset:X)])).

zero_syll *> (word,
	      ((sing:append(F,[(ecv, onset:O, nucl:(vowel, atr:A, core:C1))]),
		plur:append(F,[(ecv, onset:O, nucl:(vowel, atr:A, core:C1)), (syll, nucl:atr:A)])))).

syll_zero *> (word,
	      sing:append(F,[(syll, onset:O, nucl:(atr:A1, core:C)), (syll, nucl:atr:A1)]),
	      plur:append(F,[(syll, onset:O, nucl:(atr:A1, core:C))])).


equal *> (word,
	  ((sing:append(F,[(syll, nucl:core:C2), (cvc, onset:O1, nucl:N, coda:C1)]),
	    plur:append(F,[(syll, nucl:core:C2), (cvc, onset:O1, nucl:N, coda:C1)]));
	   (sing:append(F,[(syll, nucl:core:C2), (ecv, onset:O1, nucl:N)]),
	    plur:append(F,[(syll, nucl:core:C2), (ecv, onset:O1, nucl:N)])))).

onset_onset *> (word,
		sing:append(F,[(syll), (syll, nucl:N)]),
		plur:append(F,[(syll), (syll, nucl:N)])).

syll_syll *> (word,
	      sing:append(F,[(syll, onset:O, nucl:atr:A1), (syll, nucl:atr:A)]),
	      plur:append(F,[(syll, onset:O, nucl:atr:A1), (syll, nucl:atr:A)])).

%% allows for dipth and lengthening

%% no need for extra stem properties since we have to define it extra
%% x_iina *> (word,
%% 	   sing:append(F,[(syll, onset:O)]),
%% 	   plur:append(F,[(syll, onset:O, nucl: (atr:A, @vow_ln(i))),
%% 			  (syll, onset: @ph(n),
%% 			   nucl: (atr:A, @vow_sn(a)))])).

sg_u *> @sg_x(u).
sg_a *> @sg_x(a).
sg_o *> @sg_x(o).
sg_i *> @sg_x(i).
sg_e *> @sg_x(e).

sg_og *> @sg_ox(g).
sg_ong *> @sg_ox(ng).
sg_on *> @sg_ox(n).
sg_or *> @sg_ox(r).

pl_u *> @pl_x(u).
pl_a *> @pl_x(a).
pl_i *> @pl_x(i).
pl_e *> @pl_x(e).

pl_or *> @pl_ox(r).
pl_on *> @pl_ox(n).

pl_cn *> @pl_cx(n).
pl_cm *> @pl_cx(m).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% V-V
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% bolo₁  [ō-ō] pl. bwəəlu, bwəllu
l_bolo1 ---> (o_u_v1_v2_vv_dp,
	      sing:[(cv, onset: @ph(b), nucl: @vow_s(o, plus)),
		    (cv, onset: @ph(l), nucl: @vow_s(o, plus))]).

l_bolo2 ---> (o_u_v1_v2_cc_dp,
	      sing:[(cv, onset: @ph(b), nucl: @vow_s(o, plus)),
		    (cv, onset: @ph(l), nucl: @vow_s(o, plus))]).

%% cawe cawə

l_cawe1 ---> (e_a_v1_v2_regular,
	      sing:[(cv, onset: @ph(c), nucl: @vow_s(a, plus)),
		    (cv, onset: @ph(w), nucl: @vow_s(e, plus))]).

l_cawe2 ---> (e_a_v1_v2_regular,
	      plur:[(cv, onset: @ph(c), nucl: @vow_s(a, plus)),
		    (cv, onset: @ph(w), nucl: @vow_s(a, plus))]).

%% pʋpɔnɔ         pʋpwaanʋ

l_pupono1 ---> (o_u_v1_v2_vv_dp,
		sing:[(cv, onset: @ph(p), nucl: @vow_s(u, minus)),
		      (cv, onset: @ph(p), nucl: @vow_s(o, minus)),
		      (cv, onset: @ph(n), nucl: @vow_s(o, minus))]).

l_pupono2 ---> (o_u_v1_v2_vv_dp,
		plur:[(cv, onset: @ph(p), nucl: @vow_s(u, minus)),
		      (cv, onset: @ph(p), nucl: @vow_l(wa, minus)),
		      (cv, onset: @ph(n), nucl: @vow_s(u, minus))]).

%% tapwal-bu tapwal-biə

l_tapwalbu1 ---> (u_a_v1_v2_regular,
		  sing:[(syll, onset: @ph(t), nucl: @vow_s(a, minus)),
			(syll, onset: @ph(p), nucl: @vow_l(wa, minus), coda: @ph(l)),
			(syll, onset: @ph(b), nucl: @vow_s(u, plus))]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% zero_syll
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% aliəfuu  aliəfuuru

l_aliafuu1 ---> (u_zero_syll_regular, 
		 sing:[(syll, nucl: @vow_s(a, plus)),
		       (syll, onset: @ph(l), nucl: @vow_gl(i, a, plus)),
		       (cv, onset: @ph(f), nucl: @vow_l(u, plus))]).

%% ga-naa ga-naanɩ

l_ganaa1 ---> (i_zero_syll_regular, 
	       sing:[(syll, onset: @ph(g), nucl: @vow_s(a, minus)),
		     (syll, onset: @ph(n), nucl: @vow_l(a, minus))]).

%% dɩɩ   dɩɩna

l_dii1 ---> (a_zero_syll_regular, 
	     sing:[(syll, onset: @ph(d), nucl: @vow_l(i, minus))]).

%% dudu duduurə

l_dudu1 ---> (a_zero_syll_vv_or, 
	      sing:[(syll, onset: @ph(d), nucl: @vow_s(u, plus)),
		    (syll, onset: @ph(d), nucl: @vow_s(u, plus))]).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% syll_zero
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% laancɩga laancɩ

l_laanciga1 ---> (a_syll_zero_regular_og,
		  sing:[(syll, onset: @ph(l), nucl: @vow_l(a, minus), coda: @ph(n)),
			(syll, onset: @ph(c), nucl: @vow_s(i, minus)),
			(syll, onset: @ph(g), nucl: @vow_s(a, minus))]).

l_laanciga2 ---> (a_syll_zero_regular_og, 
		  plur:[(syll, onset: @ph(l), nucl: @vow_l(a, minus), coda: @ph(n)),
			(syll, onset: @ph(c), nucl: @vow_s(i, minus))]).


%% jɩŋa     jɩ

l_jinga1 ---> (a_syll_zero_regular_ong,
	       sing:[(syll, onset: @ph(j), nucl: @vow_s(i, minus)),
		     (syll, onset: @ph(ng), nucl: @vow_s(a, minus))]).

l_jinga2 ---> (a_syll_zero_regular_ong,
	       plur:[(syll, onset: @ph(j), nucl: @vow_s(i, minus))]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% syll_coda
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% kapɩna    kapɩm

l_kapina1 ---> (a_syll_zero_on_cm,
		sing:[(syll, onset: @ph(k), nucl: @vow_s(a, minus)),
		      (syll, onset: @ph(p), nucl: @vow_s(i, minus)),
		      (syll, onset: @ph(n), nucl: @vow_s(a, minus))]).

%% kapa-sɩŋa kapa-sɩn

l_kapasinga1 ---> (a_syll_zero_ong_cn,
		   sing:[(syll, onset: @ph(k), nucl: @vow_s(a, minus)),
			 (syll, onset: @ph(p), nucl: @vow_s(a, minus)),
			 (syll, onset: @ph(s), nucl: @vow_s(i, minus)),
			 (syll, onset: @ph(ng), nucl: @vow_s(a, minus))]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% equal
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% namarɩ   namarɩ

l_namari1 ---> (equal_regular,
		sing:[(ecv, onset: @ph(n), nucl: @vow_s(a, minus)),
		      (ecv, onset: @ph(m), nucl: @vow_s(a, minus)),
		      (ecv, onset: @ph(r), nucl: @vow_s(i, minus))]).

%% kog-kɔm kog-kwam

%% wo-vəlu wo-vəəlu

l_wovalu1 ---> (equal_vv,
		sing:[(ecv, onset: @ph(w), nucl: @vow_s(o, minus)),
		      (ecv, onset: @ph(v), nucl: @vow_s(a, minus)),
		      (ecv, onset: @ph(l), nucl: @vow_s(u, minus))]).

%% yi-kunu yi-kunnu

l_yikunu1 ---> (equal_cc,
		sing:[(ecv, onset: @ph(y), nucl: @vow_s(i, minus)),
		      (ecv, onset: @ph(k), nucl: @vow_s(u, minus)),
		      (ecv, onset: @ph(n), nucl: @vow_s(u, minus))]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% onset-onset
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% cɩcʋgʋ     cɩkʋrʋ

l_cicugu1 ---> (onset_onset_og_or_regular,
		sing:[(ecv, onset: @ph(c), nucl: @vow_s(i, minus)),
		      (ecv, onset: @ph(c), nucl: @vow_s(u, minus)),
		      (ecv, onset: @ph(g), nucl: @vow_s(u, minus))]).

%% twɩzuŋə    twɩzunə

l_twizunga1 ---> (onset_onset_ong_on_regular,
		  sing:[(syll, onset: @ph(t), nucl: @vow_s(i, plus)),
			(ecv, onset: @ph(z), nucl: @vow_s(u, plus)),
			(ecv, onset: @ph(ng), nucl: @vow_s(a, plus))]).

%% pɩnyaŋa   pɩnyaana

l_pinyanga1 ---> (onset_onset_ong_on_vv,
		  sing:[(syll, onset: @ph(p), nucl: @vow_s(i, plus), coda: @ph(n)),
			(syll, onset: @ph(y), nucl: @vow_s(a, plus)),
			(syll, onset: @ph(ng), nucl: @vow_s(a, plus))]).

%% ŋwam-pʋgʋ ŋwam-pʋrrʋ

l_ngwampugu1 ---> (onset_onset_og_or_cc,
		   sing:[(syll, onset: @ph(ng), nucl: @vow_s(wa, minus), coda: @ph(m)),
			 (syll, onset: @ph(p), nucl: @vow_s(u, minus)),
			 (syll, onset: @ph(g), nucl: @vow_s(u, minus))]).

%% bu-dʋŋʋ   bu-dʋnnʋ

l_budungu1 ---> (onset_onset_ong_on_cc,
		 sing:[(syll, onset: @ph(b), nucl: @vow_s(u, minus)),
		       (syll, onset: @ph(d), nucl: @vow_s(u, minus)),
		       (syll, onset: @ph(ng), nucl: @vow_s(u, minus))]).

l_budungu2 ---> (onset_onset_ong_on_cc,
		 plur:[(syll, onset: @ph(b), nucl: @vow_s(u, minus)),
		       (syll, onset: @ph(d), nucl: @vow_s(u, minus), coda: @ph(n)),
		       (syll, onset: @ph(n), nucl: @vow_s(u, minus))]).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% syll-syll
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% patɔɔrʋ    patɔɔna

l_patooru1 ---> (u_a_syll_syll_or_on_regular,
		 sing:[(syll, onset: @ph(p), nucl: @vow_s(a, minus)),
		       (syll, onset: @ph(t), nucl: @vow_l(o, minus)),
		       (syll, onset: @ph(r), nucl: @vow_s(u, minus))]).

l_patooru2 ---> (u_a_syll_syll_or_on_regular,
		 plur:[(syll, onset: @ph(p), nucl: @vow_s(a, minus)),
		       (syll, onset: @ph(t), nucl: @vow_l(o, minus)),
		       (syll, onset: @ph(n), nucl: @vow_s(a, minus))]).
